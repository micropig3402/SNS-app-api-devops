#!/bin/bash

# EC2上のAmazon LinuxにDockerをインストール
sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user # ec2-userをdocker使用可能グループに入れる