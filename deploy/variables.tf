variable "prefix" {
  default = "saad"
}

variable "project" {
  default = "sns-app-api-devops"
}

variable "contact" {
  default = "email@tonpei.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "sns-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "303390340303.dkr.ecr.us-east-1.amazonaws.com/sns-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "303390340303.dkr.ecr.us-east-1.amazonaws.com/sns-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}