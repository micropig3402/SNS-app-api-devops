#!/bin/sh

set -e

python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate

# 9000番ポートでuwsgiを起動　workerはサーバのメモリとリソース次第 
# --moduleのアプリケーションを実際に実行している
#「apiアプリのwsgi.pyの置いてあるフォルダの名前を指定」
uwsgi --socket :9000 --workers 4 --master --enable-threads --module sns.wsgi